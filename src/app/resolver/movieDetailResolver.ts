import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { MovieSearchService } from './movie-search.service';

@Injectable()
export class MovieDetailResolver implements Resolve<any> {
  constructor(private movieSearchService: MovieSearchService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.movieSearchService.getMovie(route.params.id);
  }
}