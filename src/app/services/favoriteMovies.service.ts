import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FavoriteMoviesService {
  constructor() {}

  getFavorites() {
    // utiliser localStorage ou sessionStorage pour récupérer les films enregistrés en tant que favoris
  }

  addToFavorites(movie) {
    // utiliser localStorage ou sessionStorage pour ajouter un film aux favoris
  }

  removeFromFavorites(movieId) {
    // utiliser localStorage ou sessionStorage pour supprimer un film des favoris
  }
}



