import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private data = new BehaviorSubject<number>(0);
  currentData = this.data.asObservable();

  constructor() { }

  updateData(data: number) {
    this.data.next(data);
  }
}
