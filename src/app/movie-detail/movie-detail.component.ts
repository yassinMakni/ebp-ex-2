import { Component, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent  implements OnInit {
 
  constructor(private route: ActivatedRoute) {
    this.route.data.subscribe(data => {
      this.movie = data.movie;
    });
  }

  like() {
    // utiliser localStorage ou sessionStorage pour enregistrer le film comme aimé
  }

  addToFavorites() {
    // utiliser localStorage ou sessionStorage pour ajouter le film aux favoris
  }
}
